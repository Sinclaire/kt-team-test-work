<?php

namespace Sinclaire\Todolist\Plugin;

use Sinclaire\Todolist\Api\Data\TodoItemInterface;

class TodoItemRepositoryPlugin
{
    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * TodoItemRepositoryPlugin constructor.
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     */
    public function __construct(
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
    ) {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param \Sinclaire\Todolist\Api\TodoRepositoryInterface $subject
     * @param \Magento\Framework\Api\SearchResults $searchResults
     * @return \Magento\Framework\Api\SearchResults
     */
    public function afterGetList(
        \Sinclaire\Todolist\Api\TodoRepositoryInterface $subject,
        \Magento\Framework\Api\SearchResults $searchResults
    ) {
        /** @var \Sinclaire\Todolist\Api\Data\TodoItemInterface $todoItem */
        foreach ($searchResults->getItems() as $todoItem) {
            $this->setUserToTodoItem($todoItem);
        }
        return $searchResults;
    }

    /**
     * @param \Sinclaire\Todolist\Api\TodoRepositoryInterface $subject
     * @param \Sinclaire\Todolist\Api\Data\TodoItemInterface $todoItem
     * @return \Sinclaire\Todolist\Api\Data\TodoItemInterface
     */
    public function afterGetById(
        \Sinclaire\Todolist\Api\TodoRepositoryInterface $subject,
        \Sinclaire\Todolist\Api\Data\TodoItemInterface $todoItem
    ) {
        $this->setUserToTodoItem($todoItem);
        return $todoItem;
    }

    private function setUserToTodoItem(TodoItemInterface $todoItem)
    {
        if (!$todoItem->getCustomerId()) {
            return;
        }

        $customer = $this->customerRepository->getById($todoItem->getCustomerId());

        $todoItem->setCustomer($customer);
    }
}