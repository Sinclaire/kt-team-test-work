<?php

namespace Sinclaire\Todolist\Model\ResourceModel\TodoItem;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            \Sinclaire\Todolist\Model\TodoItem::class,
            \Sinclaire\Todolist\Model\ResourceModel\TodoItem::class
        );
    }
}