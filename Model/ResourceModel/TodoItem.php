<?php
namespace Sinclaire\Todolist\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class TodoItem extends AbstractDb
{
    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('sinclaire_todolist_todoitem', 'sinclaire_todolist_todoitem_id');
    }
}