<?php

namespace Sinclaire\Todolist\Model;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\StateException;
use Sinclaire\Todolist\Api\Data\TodoItemInterface;
use Sinclaire\Todolist\Api\TodoRepositoryInterface;
use Sinclaire\Todolist\Model\ResourceModel\TodoItem\CollectionFactory;

class TodoRepository implements TodoRepositoryInterface
{
    /**
     * @var TodoItemInterfaceFactory
     */
    private $itemFactory;

    /**
     * @var ResourceModel\TodoItem
     */
    private $resource;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;
    /**
     * @var TodoItemSearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * TodoRepository constructor.
     * @param \Sinclaire\Todolist\Api\Data\TodoItemInterfaceFactory $itemFactory
     * @param \Sinclaire\Todolist\Model\ResourceModel\TodoItem $resource
     * @param \Sinclaire\Todolist\Model\ResourceModel\TodoItem\CollectionFactory $collectionFactory
     * @param \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface $collectionProcessor
     * @param \Sinclaire\Todolist\Api\Data\TodoItemSearchResultsInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        \Sinclaire\Todolist\Api\Data\TodoItemInterfaceFactory $itemFactory,
        \Sinclaire\Todolist\Model\ResourceModel\TodoItem $resource,
        \Sinclaire\Todolist\Model\ResourceModel\TodoItem\CollectionFactory $collectionFactory,
        CollectionProcessorInterface $collectionProcessor,
        \Sinclaire\Todolist\Api\Data\TodoItemSearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->itemFactory = $itemFactory;
        $this->resource = $resource;
        $this->collectionFactory = $collectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Sinclaire\Todolist\Api\Data\TodoItemSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->collectionFactory->create();

        $this->collectionProcessor->process($searchCriteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }

    /**
     * @param int $id
     * @return TodoItemInterface
     */
    public function getById($id)
    {
        $todoItem = $this->itemFactory->create();
        $this->resource->load($todoItem, $id);

        return $todoItem;
    }

    /**
     * @param \Sinclaire\Todolist\Api\Data\TodoItemInterface $todoItem
     * @return \Sinclaire\Todolist\Api\Data\TodoItemInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Sinclaire\Todolist\Api\Data\TodoItemInterface $todoItem)
    {
        try {
            $this->resource->save($todoItem);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(
                __(
                    'Could not save to do item: %1',
                    $e->getMessage()
                ),
                $e
            );
        }

        return $todoItem;
    }

    /**
     * @param $id
     * @return bool
     * @throws StateException
     */
    public function deleteById($id)
    {
        $todoItem = $this->getById($id);
        return  $this->delete($todoItem);
    }

    /**
     * @param \Sinclaire\Todolist\Api\Data\TodoItemInterface $todoItem
     * @return bool
     * @throws StateException
     */
    public function delete(\Sinclaire\Todolist\Api\Data\TodoItemInterface $todoItem)
    {
        try {
            $this->resource->delete($todoItem);
        } catch (\Exception $e) {
            throw new StateException(
                __(
                    'Cannot delete to do item with id %1',
                    $todoItem->getId()
                ),
                $e
            );
        }
        return true;
    }
}