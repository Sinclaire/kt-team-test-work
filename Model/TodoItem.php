<?php

namespace Sinclaire\Todolist\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use Sinclaire\Todolist\Api\Data\TodoItemInterface;

class TodoItem extends AbstractModel implements TodoItemInterface, IdentityInterface
{
    const CACHE_TAG = 'todo_item';

    /**
     * @var \Magento\Customer\Api\Data\CustomerInterface
     */
    private $customer;

    protected function _construct()
    {
        $this->_init(\Sinclaire\Todolist\Model\ResourceModel\TodoItem::class);
    }

    public function getTitle()
    {
        return $this->getData('title');
    }

    /**
     * @param string $title
     * @return \Sinclaire\Todolist\Api\Data\TodoItemInterface
     */
    public function setTitle(string $title)
    {
        return $this->setData('title', $title);
    }

    /**
     * @return int|mixed
     */
    public function getCustomerId()
    {
        return $this->getData('user_id');
    }

    /**
     * @param int $id
     * @return self
     */
    public function setCustomerId(int $id)
    {
        $this->setData('user_id', $id);
        return $this;
    }

    /**
     * @return \Magento\Customer\Api\Data\CustomerInterface
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param \Magento\Customer\Api\Data\CustomerInterface $customer
     * @return self
     */
    public function setCustomer(\Magento\Customer\Api\Data\CustomerInterface $customer)
    {
        $this->customer = $customer;

        $this->setData('user_id', $customer->getId());

        return $this;
    }

    public function hasCustomer()
    {
        return isset($this->customer);
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}