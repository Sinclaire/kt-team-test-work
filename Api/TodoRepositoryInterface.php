<?php
namespace Sinclaire\Todolist\Api;

interface TodoRepositoryInterface
{
    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Sinclaire\Todolist\Api\Data\TodoItemSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * @param int $id
     * @return \Sinclaire\Todolist\Api\Data\TodoItemInterface
     */
    public function getById($id);

    /**
     * @param \Sinclaire\Todolist\Api\Data\TodoItemInterface $todoItem
     * @return \Sinclaire\Todolist\Api\Data\TodoItemInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Sinclaire\Todolist\Api\Data\TodoItemInterface $todoItem);

    /**
     * @param int $id
     * @return bool
     */
    public function deleteById($id);

    /**
     * @param \Sinclaire\Todolist\Api\Data\TodoItemInterface $todoItem
     * @return bool
     */
    public function delete(\Sinclaire\Todolist\Api\Data\TodoItemInterface $todoItem);
}