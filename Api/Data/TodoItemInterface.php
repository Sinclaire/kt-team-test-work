<?php
namespace Sinclaire\Todolist\Api\Data;

interface TodoItemInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @param mixed $id
     * @return self
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param string $title
     * @return self
     */
    public function setTitle(string $title);

    /**
     * @return int
     */
    public function getCustomerId();

    /**
     * @param int $id
     * @return self
     */
    public function setCustomerId(int $id);

    /**
     * @return \Magento\Customer\Api\Data\CustomerInterface
     */
    public function getCustomer();

    /**
     * @param \Magento\Customer\Api\Data\CustomerInterface $customer
     * @return self
     */
    public function setCustomer(\Magento\Customer\Api\Data\CustomerInterface $customer);

    /**
     * @return bool
     */
    public function hasCustomer();
}