<?php

namespace Sinclaire\Todolist\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface TodoItemSearchResultsInterface extends SearchResultsInterface
{
    /**
     * @return \Sinclaire\Todolist\Api\Data\TodoItemInterface[]
     */
    public function getItems();

    /**
     * @param \Sinclaire\Todolist\Api\Data\TodoItemInterface[] $items
     * @return $this
     */
    public function setItems(array $items = null);
}