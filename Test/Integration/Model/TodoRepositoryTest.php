<?php
namespace Sinclaire\Todolist\Test\Integration\Model;

use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroupBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\TestFramework\Helper\Bootstrap;
use Sinclaire\Todolist\Api\Data\TodoItemInterface;
use Sinclaire\Todolist\Api\TodoRepositoryInterface;

class TodoRepositoryTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var TodoRepositoryInterface
     */
    private $repository;

    protected function setUp()
    {
        $this->repository = Bootstrap::getObjectManager()->create(TodoRepositoryInterface::class);
    }

    public static function loadFixtures()
    {
        require_once __DIR__ .'/../_files/dummy_todo_item.php';
    }

    /**
     * @magentoDbIsolation enabled
     * @magentoAppIsolation enabled
     */
    public function testAddTodoItem()
    {
        /** @var TodoItemInterface $todoItem */
        $todoItem = Bootstrap::getObjectManager()->create(TodoItemInterface::class);
        $todoItem->setTitle('First');
        $todoItemSaved = $this->repository->save($todoItem);

        $this->assertEquals('First', $todoItemSaved->getTitle());
    }


    /**
     * @magentoDbIsolation enabled
     * @magentoAppIsolation enabled
     * @magentoDataFixture loadFixtures
     */
    public function testGetTodoItemByCustomerId()
    {
        /** @var SearchCriteriaBuilder $searchCriteriaBuilder */
        $searchCriteriaBuilder = Bootstrap::getObjectManager()->create(SearchCriteriaBuilder::class);
        $filterBuilder = Bootstrap::getObjectManager()->create(FilterBuilder::class);
        $filterGroupBuilder = Bootstrap::getObjectManager()->create(FilterGroupBuilder::class);

        $filter = $filterBuilder->setField('user_id')
            ->setConditionType('eq')
            ->setValue(1)
            ->create();
        $filterGroup = $filterGroupBuilder
            ->addFilter($filter)
            ->create();
        $searchCriteria = $searchCriteriaBuilder->setFilterGroups([$filterGroup])
            ->create();

        $todoItems = $this->repository->getList($searchCriteria)->getItems();
        $todoItem = array_shift($todoItems);

        $this->assertEquals('Example ToDo Item', $todoItem->getTitle());
    }
}