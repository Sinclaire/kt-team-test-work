<?php

/** @var \Magento\Customer\Api\Data\CustomerInterface $customer */
$customer = \Magento\TestFramework\Helper\Bootstrap::getObjectManager()->create(
    \Magento\Customer\Api\Data\CustomerInterface::class
);

/** @var \Magento\Customer\Api\CustomerRepositoryInterface $customerRepo */
$customerRepo = \Magento\TestFramework\Helper\Bootstrap::getObjectManager()->create(
    \Magento\Customer\Api\CustomerRepositoryInterface::class
);

$customer->setId(1)
    ->setWebsiteId(1)
    ->setEmail('customer@example.com')
    ->setGroupId(1)
    ->setStoreId(1)
    ->setPrefix('Mr.')
    ->setFirstname('John')
    ->setMiddlename('A')
    ->setLastname('Smith')
    ->setSuffix('Esq.')
    ->setDefaultBilling(1)
    ->setDefaultShipping(1)
    ->setTaxvat('12')
    ->setGender(0);

$customerRepo->save($customer);

/** @var \Sinclaire\Todolist\Api\Data\TodoItemInterface $todoItem */
$todoItem = \Magento\TestFramework\Helper\Bootstrap::getObjectManager()->create(
    \Sinclaire\Todolist\Api\Data\TodoItemInterface::class
);

$todoItem->setTitle('Example ToDo Item');
$todoItem->setCustomerId($customer->getId());

$todoRepo = \Magento\TestFramework\Helper\Bootstrap::getObjectManager()->create(
    \Sinclaire\Todolist\Api\TodoRepositoryInterface::class
);

$todoRepo->save($todoItem);