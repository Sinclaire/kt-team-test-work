<?php

namespace Sinclaire\Todolist\Controller\Add;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Session;
use Sinclaire\Todolist\Api\Data\TodoItemInterface;
use Sinclaire\Todolist\Api\TodoRepositoryInterface;
use Sinclaire\Todolist\Model\TodoItemFactory;

class Index extends Action
{
    /**
     * @var \Sinclaire\Todolist\Api\Data\TodoItemInterfaceFactory
     */
    private $todoItemFactory;

    /**
     * @var \Sinclaire\Todolist\Api\TodoRepositoryInterface
     */
    private $todoRepository;
    /**
     * @var Session
     */
    private $session;

    /**
     * Index constructor.
     * @param Context $context
     * @param \Sinclaire\Todolist\Api\Data\TodoItemInterfaceFactory $todoItemFactory
     * @param \Sinclaire\Todolist\Api\TodoRepositoryInterface $todoRepository
     * @param Session $session
     */
    public function __construct(
        Context $context,
        \Sinclaire\Todolist\Api\Data\TodoItemInterfaceFactory $todoItemFactory,
        \Sinclaire\Todolist\Api\TodoRepositoryInterface $todoRepository,
        Session $session
    ) {
        parent::__construct($context);
        $this->todoItemFactory = $todoItemFactory;
        $this->todoRepository = $todoRepository;
        $this->session = $session;
    }

    public function execute()
    {
        $title = $this->getRequest()->getParam('title');

        if (!isset($title)) {
            return $this->_redirect('todolist/index');
        }

        $item = $this->todoItemFactory->create();
        $item->setTitle($title);

        if ($this->session->isLoggedIn()) {
            $customerId = $this->session->getCustomerId();
            $item->setCustomerId($customerId);
        }

        $this->todoRepository->save($item);

        return $this->_redirect('todolist/index');
    }
}