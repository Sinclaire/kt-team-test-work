<?php

namespace Sinclaire\Todolist\Block;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\View\Element\Template;
use Sinclaire\Todolist\Api\TodoRepositoryInterface;

class Index extends Template
{
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var TodoRepositoryInterface
     */
    private $todoRepository;

    /**
     * Index constructor.
     * @param Template\Context $context
     * @param TodoRepositoryInterface $todoRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        TodoRepositoryInterface $todoRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->todoRepository = $todoRepository;
    }

    public function getTodoList()
    {
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $result = $this->todoRepository->getList($searchCriteria);

        return $result->getItems();
    }
}